@component('mail::message')
# Your order is confirmed

@component('mail::table')

| Product       | Amount         | Price  |
| ------------- |:-------------:| --------:|
@foreach ($cart as $item)
|  {{$item['name']}}     | {{$item['quantity']}}      | {{$item['price']}} €     |
@endforeach

@endcomponent

The body of your message.




@endcomponent
