<?php

use App\Http\Controllers\dashboard;
use App\Http\Controllers\ProductsTableController;
use App\Http\Controllers\store;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', [store::class, 'index'])->name('store');
Route::post('/addtocart', [store::class, 'addtocart'])->name('addcart');
Route::get('/cart', [store::class, 'cart'])->name('cart');
Route::post('/updatecart', [store::class, 'updatecart'])->name('updatecart');
Route::post('/deleteitem', [store::class, 'deleteitem'])->name('deleteitem');
Route::post('/subscribe', [store::class, 'subscribe'])->name('subscribe');
Route::post('/success', [store::class, 'success'])->name('success');

Route::get('/mailable', function () {
    $cart = session('cart');

    return new App\Mail\Paymentcomplete($cart);
});


Route::get('/dashboard', [dashboard::class, 'index'])->middleware(['auth'])->name('dashboard');
Route::get('/products', [ProductsTableController::class, 'index'])->middleware(['auth'])->name('products');
Route::get('/products/add', [ProductsTableController::class, 'create'])->middleware(['auth'])->name('products.add');
Route::post('/products/store', [ProductsTableController::class, 'store'])->middleware(['auth'])->name('products.store');
Route::get('/products/edit/{id}', [ProductsTableController::class, 'edit'])->middleware(['auth'])->name('products.edit');
Route::post('/products/update', [ProductsTableController::class, 'update'])->middleware(['auth'])->name('products.update');
Route::post('/products/delete', [ProductsTableController::class, 'destroy'])->middleware(['auth'])->name('products.delete');

require __DIR__.'/auth.php';
