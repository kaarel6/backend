<?php

namespace App\Http\Controllers;

use App\Mail\Paymentcomplete;
use App\Models\Products_table;
use Illuminate\Http\Client\Request as ClientRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Stripe\PaymentIntent;
use Stripe\Stripe;

class store extends Controller
{
    public function index()
    {
        return view('welcome' , [
            'products' => Products_table::all(),
            'cart' => session('cart', [])
        ]);
    }

    public function cart()
    {
        return view('cart', [
            'cart' => session('cart')
        ]);
    }

    public function addtocart(Request $request)
    {
        $cart = $request->session()->get('cart');

        $quantity = 0;
        if($request->qty <= 0){
            $quantity = 1;
        }else{
            $quantity = $request->quantity;
        }

        $product = Products_table::find($request->id);
        $productitem = [
            'id' => $product->id,
            'name' => $product->name,
            'description' => $product->description,
            'price' => $product->price,
            'image' => $product->image,
            'quantity' => $quantity
        ];
        if (!$cart) {
            $cart = [
                $product->id => $productitem
            ];
            $request->session()->put('cart', $cart);
            return redirect()->back();
        }
        if (isset($cart[$product->id])) {
            $cart[$product->id]['quantity']++;
            $request->session()->put('cart', $cart);
            return redirect()->back();
        }
        $cart[$product->id] = $productitem;
        $request->session()->put('cart', $cart);
        return redirect()->back();
    }
    public function updatecart(Request $request){
        $quantity = 0;
        if($request->quantity <= 0){
            $quantity = 1;
        }else{
            $quantity = $request->quantity;
        }
        $cart = session('cart');
        $cart[$request->id]['quantity'] = $quantity;
        $request->session()->put('cart', $cart);
        return redirect()->back();
    }
    public function deleteitem(Request $request){
        $cart = session('cart');
        unset($cart[$request->id]);
        $request->session()->put('cart', $cart);
        return redirect()->back();
    }
    public function subscribe(){
        $cart = collect(session('cart'));
        $total = $cart->map(function ($item) {
            return intval(($item['price'] * 100) * $item['quantity']);
        });

        Stripe::setApiKey('sk_test_51KH68LElkTHhFkpt2cS6tUPHYLzQEeA4cwjCbYx8dItmwygPAliYskrwfmXxQMcra73zCJMIWtuJSd8kYOtl3CHU00evWifhAY');
        $paymentIntent = PaymentIntent::create([
            'amount' => $total->sum(),
            'currency' => 'eur',
            'payment_method_types' => [
                "card"
            ],
        ]);
        $output = [
            'paymentIntent' => $paymentIntent,
            'stripePublicKey' => "pk_test_51KH68LElkTHhFkptzMSgq6q1AEKI1ZK1JpS1QtQweiQyyznB108uuMCl3MQgZ5xyrUaHeK3RlrZSMG9IWaRM53IP00urne4ZFt"
        ];
        return $output;
    }

    public function success() {
        $cart = session('cart');
        Mail::to('test@test.com')->send(new Paymentcomplete($cart));
        session()->forget('cart');
    }
}
